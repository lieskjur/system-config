#!/bin/sh
# sublime-text a sublime-merge installation from the stable channel

curl -O https://download.sublimetext.com/sublimehq-pub.gpg && sudo pacman-key --add sublimehq-pub.gpg && sudo pacman-key --lsign-key 8A8F901A && rm sublimehq-pub.gpg
echo -e "\nServer = https://download.sublimetext.com/arch/stable/x86_64" | sudo tee -a /etc/pacman.conf
sudo pacman -S --noconfirm --needed sublime-text
sudo pacman -S --noconfirm --needed sublime-merge
