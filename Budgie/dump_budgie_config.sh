#!/bin/sh
# Backup Budgie configuration
dconf dump /org/gnome/desktop/ > gnome_desktop_config
dconf dump /com/solus-project/ > budgie_config
dconf dump / > full_config
