#!/bin/bash
#set -e

func_backup() {
	echo "backing up ~/.config/"$1
	cp -R ~/.config/$1 applications
}

echo "Backing up application configs"
echo

list=(
	keepassxc
)

for name in "${list[@]}" ; do
	func_backup $name
done

