#!/bin/bash
#set -e


func_install() {
	if pacman -Qi $1 &> /dev/null; then
		tput setaf 2
  		echo $1 "is already installed"
		tput sgr0
	else
    	tput setaf 3
    	echo "Installing" $1
    	echo
    	tput sgr0
    	sudo pacman -S --noconfirm --needed $1 
    fi
}

echo "Installation of additional software"

list=(
	timeshift
	keepassxc
	julia
	deluge
	flameshot
)

for name in "${list[@]}" ; do
	echo
	func_install $name
done

